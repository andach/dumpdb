# dumpdb

A project to add two artisan commands:

* php artisan dumpdb --setup=false
* php artisan importdb --setup=false

These commands would run the setup if it had not been run, regardless of the flag. 

## Setup Function
The setup function would need to be run to declare which of the tables need to be considered for anonymisation. 

The user can choose the tables through the CLI, and then declare which ones of them are addresses, people, etc. 

Then the columns can be selected to be overridden. 

